## Student Author and Contact Info
Newton Blair 
email: nblair@uoregon.edu


## Software Description
This software is an extenstion of my Project 5. For it I utilize 4 containers one for the database, one for the the laptop-service, one for the web, and one for the website. Upon Starting this software go to 127.0.0.1:5000 this will take you to the acp calculator where you will input your km checkpoint distances then press the submit button to send the data to the database. After that you can access the data in a variety of ways from the api port or the web port. The specifics to access the data is described in specifications.

## Specifications
As described above to begin using the software after the containers are up and running put 127.0.0.1:5000 in your browser and it should render the calculator page. Enter the data you would like to submit to the database and press the submit button at the bottom of the page.

Once submitted you can access this data using either the api port of 127.0.0.1:5001 or the consumer port of 127.0.0.1:5002.
    -If using the consumer port of 127.0.0.1:5002 upon entering this to your browser should automatically render a page prompting you to select a value and submit it to access the top x  open and close times from your data. If you select a value and press submit it will direct you to the php page located at 127.0.0.1:5002/top.php?top=x where x is the value that you selected. From here you can alter the value of top in the url to change the number of values that get displayed once refreshing the page. Entering a number bigger than the number of data in your database will just render all the data from the database. The top.php page shows the listOpenOnly and listCloseOnly values in both json and csv format.
        -Otherwise if you click the link below the prompt it will direct you to 127.0.0.1:5002/listAll.php This page contains all the version calls to the api that do not involve a top argument. This page display the listAll, listOpenOnly, and listCloseOnly only info. in both json and csv formats. 

    -If using the api port of 127.0.0.1:5001 nothing will display unless you use one of the correlating extensions.
        "http://<host:port>/listAll" should return all open and close times in the database
        "http://<host:port>/listOpenOnly" should return open times only
        "http://<host:port>/listCloseOnly" should return close times only

        "http://<host:port>/listAll/csv" should return all open and close times in CSV format
        "http://<host:port>/listOpenOnly/csv" should return open times only in CSV format
        "http://<host:port>/listCloseOnly/csv" should return close times only in CSV format

        "http://<host:port>/listAll/json" should return all open and close times in JSON format

        "http://<host:port>/listOpenOnly/json" should return open times only in JSON format
        "http://<host:port>/listCloseOnly/json" should return close times only in JSON format
        You will also add a query parameter to get top "k" open and close times. For examples, see below.

        "http://<host:port>/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format
        "http://<host:port>/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
        "http://<host:port>/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
        "http://<host:port>/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format

## Notes
1) Pressing submit on a form with no control points will take you to a error page asking you to fix that mistake with a button to take you back to the page.

2) Pressing on the display button before the submit button will take you to the 'finished' page, but had a note saying if this page is empty you did not hit submit and should go back and try again with a button that takes you back to the starting page. 

3)this software will not catch if the checkpoints got inserted out of order and will return out of order times if they are. Inserting the checkpoints in the correct order is how you can ensure this program functions properly. 