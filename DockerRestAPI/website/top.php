<html>
    <head>
        <title>Open and Close Times page</title>
    </head>

    <body>

        <h1>Top listOpenOnly Times Json</h1>
        <ul>
            <?php
            $num = $_GET['top'];
            if($num == Null){
                $final_url = 'http://laptop-service/listOpenOnly';
            }
            else{
                $final_url = 'http://laptop-service/listOpenOnly/json' . '?top=' . $num;
            }
            $json = file_get_contents($final_url);
            $obj = json_decode($json);
                $result = $obj;
            foreach ($result as $l) {
                echo "<li>open: $l->open</li>";
            }
            ?>
        </ul>

        <h1>Top listOpenOnly Times CSV</h1>
        <ul>
            <?php
            $num = $_GET['top'];
            if($num == Null){
                $final_url = 'http://laptop-service/listOpenOnly/json';
            }
            else{
                $final_url = 'http://laptop-service/listOpenOnly/json' . '?top=' . $num;
            }
            $json = file_get_contents($final_url);
            $obj = json_decode($json);
                $result = $obj;
                echo "<li>open</li>";
            foreach ($result as $l) {
                echo "<li>$l->open</li>";
            }
            ?>
        </ul>

        <h1>Top listCloseOnly Times Json</h1>
        <ul>
            <?php
            $num = $_GET['top'];
            if($num == Null){
                $final_url = 'http://laptop-service/listCloseOnly';
            }
            else{
                $final_url = 'http://laptop-service/listCloseOnly' . '?top=' . $num;
            }
            $json = file_get_contents($final_url); //uses final url as path to get data from
            $obj = json_decode($json);
                $result = $obj;
            foreach ($result as $l) {
                echo "<li>close: $l->close</li>";
            }
            ?>
        </ul>

        <h1>Top listCloseOnly Times CSV</h1>
        <ul>
            <?php
            $num = $_GET['top'];                               //gets num var from the url
            if($num == Null){
                $final_url = 'http://laptop-service/listCloseOnly/json';
            }
            else{
                $final_url = 'http://laptop-service/listCloseOnly/json' . '?top=' . $num;
            }
            $json = file_get_contents($final_url);
            $obj = json_decode($json);
                $result = $obj;
            echo "<li>close</li>";
            foreach ($result as $l) {
                echo "<li>$l->close</li>";
            }
            ?>
        </ul>
    </body>
</html>