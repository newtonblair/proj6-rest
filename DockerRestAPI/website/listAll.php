<html>
    <head>
        <title>Open and Close Times page</title>
    </head>

    <body>
        <h1>listAll and listAll/json</h1>
        <ol>
            <?php
            $json = file_get_contents('http://laptop-service/listAll');
            $obj = json_decode($json);
                $result = $obj;
            foreach ($result as $l) {
                echo "<li>open time: $l->open ----------> close time: $l->close</li>";
            }
            ?>
        </ol>

        <h1>listAll/csv</h1>
        <ol>
            <?php
            $json = file_get_contents('http://laptop-service/listAll');
            $obj = json_decode($json);
                $result = $obj;
            echo "open, close";
            foreach ($result as $l) {
                echo "<li>$l->open, $l->close</li>";
            }
            ?>
        </ol>

        <h1>listOpenOnly and listOpenOnly/json</h1>
        <ol>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly');
            $obj = json_decode($json);
                $result = $obj;
            foreach ($result as $l) {
                echo "<li>open time: $l->open</li>";
            }
            ?>
        </ol>

        <h1>listOpenOnly/csv</h1>
        <ol>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly');
            $obj = json_decode($json);
                $result = $obj;
            echo "open";
            foreach ($result as $l) {
                echo "<li>$l->open</li>";
            }
            ?>
        </ol>

        <h1>listCloseOnly and listCloseOnly/json</h1>
        <ol>
            <?php
            $json = file_get_contents('http://laptop-service/listCloseOnly');
            $obj = json_decode($json);
                $result = $obj;
            foreach ($result as $l) {
                echo "<li>close time: $l->close</li>";
            }
            ?>
        </ol>

        <h1>listCloseOnly/csv</h1>
        <ol>
            <?php
            $json = file_get_contents('http://laptop-service/listCloseOnly');
            $obj = json_decode($json);
                $result = $obj;
            echo "close";
            foreach ($result as $l) {
                echo "<li>$l->close</li>";
            }
            ?>
        </ol>
    </body>
</html>