<html>
    <head>
        <title>CIS 322 REST-api demo: Laptop list</title>
    </head>

    <body>
        <h1>Choose a top value you'd like for the top closing and opening times</h1>

        <p>Note once on the web page you can alter this value by changing the ?top= value in the url and refreshing the page</p>

       <form action="top.php">
           <input type="number" id="top" name="top" min="1" max="20" />
           <input onclick="window.location.href = 'https://website/top.php';" id="top" type="submit" value="submit" />
       </form>

       <p>If you would simply like to see all the times from the listAll, listCloseOnly, and listOpenOnly times <a href='/listAll.php'>click here</a></p>
    </body>
</html>