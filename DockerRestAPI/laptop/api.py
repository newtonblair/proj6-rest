# Laptop Service

from flask import Flask, request, Response
from flask_restful import Resource, Api, fields, marshal_with
from pymongo import MongoClient
import os
import csv
import logging

# Instantiate the app
app = Flask(__name__)
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

class listAll(Resource):
	allfields = {                                                     #dictionary of the keys I want in the returned data
		'open': fields.String,
		'close': fields.String
	}
	@marshal_with(allfields)                                          #filters return to only have keys from the allfields dict
	def get(self):
		_items = db.tododb.find()
		items = [item for item in _items]
		return items

class listOpenOnly(Resource):
	openfields = {
		'open': fields.String
	}
	@marshal_with(openfields)
	def get(self):
		_items = db.tododb.find()
		items = [item for item in _items]
		if request.args.get('top') is None:                           #if there is no top argument in the url just return all
			return items
		else:
			num = request.args.get('top', 0, type=int)
			resized_items = []
			if num > len(items):                                     #if the argument in the url is bigger than len of data just return avaiable data
				return items
			else:
				for i in range(num):                                 #else only get the designated num number of inputs to return
					resized_items.append(items[i])
				return resized_items


class listCloseOnly(Resource):
	closefields = {
		'close': fields.String
	}
	@marshal_with(closefields)
	def get(self):
		_items = db.tododb.find()
		items = [item for item in _items]
		if request.args.get('top') is None:
			return items
		else:
			num = request.args.get('top', 0, type=int)
			resized_items = []
			if num > len(items):
				return items
			else:
				for i in range(num):
					resized_items.append(items[i])
				return resized_items

api.add_resource(listAll, '/listAll', '/listAll/json')                              #add uri to the resources
api.add_resource(listOpenOnly, '/listOpenOnly', '/listOpenOnly/json')
api.add_resource(listCloseOnly, '/listCloseOnly', '/listCloseOnly/json')

#note: csv rewrite was based off following resource http://blog.appliedinformaticsinc.com/how-to-parse-and-convert-json-to-csv-using-python/
class listAllcsv(Resource):
	def get(self):
		r = listAll.get(listAll)                                           #get array from the listAll resource get function
		outfile = open("listAll.csv", "w")                                 #open new file in write mode
		writer = csv.writer(outfile)                                      #set writer to write into file
		count = 0
		for item in r:
			if count == 0:
				header = item.keys()                                      #gets keys from the dataset
				writer.writerow(header)                                   #write the headers as a row
				count += 1
			writer.writerow(item.values())                                #write the values as a row under correlating header
		outfile.close()
		csvfile = open("listAll.csv", "r")

		return Response(csvfile, mimetype="text/csv")                    #return the opened file in read mode

class listOpenOnlycsv(Resource):
	def get(self):
		r = listOpenOnly.get(listOpenOnly)                      #get array from the listOpenOnly resource get function
		outfile = open("listOpenOnly.csv", "w")
		writer = csv.writer(outfile)
		count = 0
		for item in r:
			if count == 0:
				header = item.keys()                             #gets keys from the dataset
				writer.writerow(header)                          #write the headers as a row
				count += 1
			writer.writerow(item.values())                       #write the values as a row under correlating header
		outfile.close()
		csvfile = open("listOpenOnly.csv", "r")

		return Response(csvfile, mimetype="text/csv")            #returns csv file

class listCloseOnlycsv(Resource):                                #resource follows same format as the above two other resources
	def get(self):
		r = listCloseOnly.get(listCloseOnly)
		outfile = open("listCloseOnly.csv", "w")
		writer = csv.writer(outfile)
		count = 0
		for item in r:
			if count == 0:
				header = item.keys()
				writer.writerow(header)
				count += 1
			writer.writerow(item.values())
		outfile.close()
		csvfile = open("listCloseOnly.csv", "r")

		return Response(csvfile, mimetype="text/csv")

api.add_resource(listAllcsv,'/listAll/csv')                             #add uri for the resources 
api.add_resource(listOpenOnlycsv, '/listOpenOnly/csv')
api.add_resource(listCloseOnlycsv, '/listCloseOnly/csv')
# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
