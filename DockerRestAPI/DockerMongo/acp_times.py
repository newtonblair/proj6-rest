"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

speedsH = {"1":(200, 34), "2": (200, 32), "3": (200, 30), "4": (400, 28), "5": (300, 26)} #dict with highspeed and distamce that speed is used for
speedsL = {"1": (200, 15), "2": (200, 15), "3": (200, 15), "4": (400, 11.428), "5": (300, 13.333)} #dict with highspeed and distamce that speed is used for
det_speeds_needed = [200, 200, 200, 400, 300] #list with distances each speed is used for
set_close_times = {0: 60, 200: 810, 300: 1200, 400: 1620, 600: 2400, 1000: 4500} #dict for the set close times in minutes

def calc_times(distance, speed):                #added function to handle repetive calculations needed
  hours = distance // speed                     #number of full hours needed to be added for given speed and distance
  remainder = distance % speed                  #remainder to be used to calculate minutes
  minutes = (remainder * 60)/speed              #number of minutes needed to be added for the given distance and speed
  return (hours, minutes)                       #returns hours and minutes seperately

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    #initialize values that will hold total hours and minutes and the start_time
    print(brevet_start_time)
    start_time = arrow.get(brevet_start_time)                              #change date_time string to arrow formatted date
    hour = 0                                                               #intialize total hours
    minute = 0                                                             #intialize total minutes

    #adjusting for time of brevet controls longer than actual distance of race
    if control_dist_km > brevet_dist_km:
      control_dist_km = brevet_dist_km

    if control_dist_km < 200:                                              #control point less than 200km only 1 speed needed
      speed = speedsH["1"][1]
      hold_hour, hold_minute = calc_times(control_dist_km, speed)          #get hours and minutes needed to be added
      hour += hold_hour                                                    #increment total hours by calculated hours
      minute += hold_minute                                                #increment total minutes by calculated minutes
    else:                                                                  #control point is 200km or more
      keys = []
      remainder = control_dist_km                                          #value to help determine how many dict sets of distances and speeds we will use
      counter = 0
      for dist in det_speeds_needed:                                       #loop through the set distances to see how many of these distance,speed sets this checkpoint encompasses
        remainder -= dist
        if remainder >= 0:                                                 #keep going while remainder remains positive
          counter += 1
          key = str(counter)
          keys.append(key)                                                 #adds key correlating to a distance,speed pair this checkpoint was determined to encompass
        else:                                                              #set distance for next speed exceeded the checkpoint, checkpoints remainder is within this speed
          remainder += dist                                                #add distance back to have correct remainder
          break                                                            #break from loop
      for key in keys:
        distance, speed = speedsH[key]                                     #using the keys obtained from above loop get hours and minutes to be added
        hold_hour, hold_minute = calc_times(distance, speed)
        hour += hold_hour                                                  #increment total hours and minutes by calculated values for the distance speed pair
        minute += hold_minute
      counter += 1                                                         #remainder requires next speed from dictionary 
      last_speed = str(counter)
      distance, speed = speedsH[last_speed]                                #just want the speed from this since remainder falls in this dictionaries range, but doesn't encompass the whole distance
      hold_hour, hold_minute = calc_times(remainder, speed)                #calculate last hours and minutes needed to be added using the remainder and its speed
      hour += hold_hour
      minute += hold_minute

    #check to see if we need to round up a minute or not
    minute_check = str(minute)                                  #make string so I can split the minute at the .
    minute_split = minute_check.split('.')
    seconds = minute_split[1]                                   #take the decimal part of the minute
    det_second = int(seconds[0])
    if det_second >= 5:                                         #more than 30 seconds over the minute so round up
      minute += 1
    minute = int(minute)                                        #changed minute to an int so seconds aren't part of time

    start_time = start_time.shift(hours =+ hour)                #increment hours of start_time by total hours
    start_time = start_time.shift(minutes =+ minute)            #incremet minutes of start_time by total minutes
    start_time = start_time.replace(tzinfo='US/Pacific')        #set time zone to Pacific
    return start_time.isoformat()                               #return open time for checkpoint


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    end_time = arrow.get(brevet_start_time)
    hour = 0
    minute = 0

    #adjusting for time of brevet controls longer than actual distance of race
    if control_dist_km > brevet_dist_km:
      control_dist_km = brevet_dist_km

    #Check for the set close time values
    if control_dist_km == brevet_dist_km or control_dist_km == 0:          #if its an end of the race checkpoint or the start point
      quick_key = int(control_dist_km)
      quick_time = set_close_times[quick_key]                              #get close time in minutes from dict using correlating key 
      end_time = end_time.shift(minutes =+ quick_time)                     #increment start time minutes by returned minute value
      end_time = end_time.replace(tzinfo='US/Pacific')
      return end_time.isoformat()                                          #return close time for checkpoint

    #determine amount of time needed to be added onto start time
    if control_dist_km < 200:                                             #control point is less than 200km so only 1 speed needed
      speed = speedsL["1"][1]
      hold_hour, hold_minute = calc_times(control_dist_km, speed)         #holder variables for the return hours and minutes
      hour += hold_hour                                                   #increment total hours by number of hours returned
      minute += hold_minute
    else:                                                                 #control point greater than 200 solve how many speeds needed
      keys = []
      remainder = control_dist_km                                         #keeps track of remainder after distances are removed to see how many distance,speed pairs this checkpoint will encompass
      counter = 0                                                         #keeps track of distances subtracted to create the keys
      for dist in det_speeds_needed:                                      #loops through distances in det_speeds_needed
        remainder -= dist                                                 #subtract each distance from the overall distance of CP
        if remainder >= 0:                                                #if the remainers is >= 0 keep looping 
          counter += 1                                                    #we use entirety of a distance so increment counter
          key = str(counter)                                              #turn counter into key
          keys.append(key)                                                #append key to keys list
        else:                                                             #subtracted distance resulted in negative remainder
          remainder += dist                                               #means distance is within that section add the distance back
          break                                                           #break from loop so it doesn't continue
      for key in keys:
        distance, speed = speedsL[key]                                    #get correlating distances and speeds from the dictionaries
        hold_hour, hold_minute = calc_times(distance, speed)              #calc hours and minutes based on the distance and speed from dict
        hour += hold_hour                                                 #increment total hours by calculated hours
        minute += hold_minute                                             #increment total minutes by calculated minutes
      counter += 1                                                        #possible remainder needs to be used with next speed
      last_speed = str(counter)
      distance, speed = speedsL[last_speed]                               #just want the speed from this set of values 
      hold_hour, hold_minute = calc_times(remainder, speed)               #calc hours and minutes for the remainder distance
      hour += hold_hour                                                   #increment hours and minutes
      minute += hold_minute

    #check to see if we need to round up a minute or not
    minute_check = str(minute)                                  #make string so I can split the minute at the .
    minute_split = minute_check.split('.')
    seconds = minute_split[1]                                   #take the decimal part of the minute
    det_second = int(seconds[0])
    if det_second >= 5:                                         #more than 30 seconds over the minute so round up
      minute += 1
    minute = int(minute)                                        #changed minute to an int so seconds aren't part of time

    #Adjust the start time to the determined close time
    end_time = end_time.shift(hours =+ hour)                    #shift hours by total number of hours for the given checkpoint
    end_time = end_time.shift(minutes =+ minute)                #shift minutes by total number of minute for given checkpoint
    end_time = end_time.replace(tzinfo='US/Pacific')            #specified time as pacific timezone
    return end_time.isoformat()                                 #returns the close adjusted time in isoformat
